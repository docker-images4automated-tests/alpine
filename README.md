# alpine

## Docker images produced by this project from alpine (5.59MB, size for v3.11)
* sudo (6.66MB)
  * bash (13.2MB)
  * openrc_easy-shutdown (8.69MB)
  * openrc_easy (8.69MB)
  * postgresql (not working)
* openrc (not working)

## OpenRC
* https://github.com/dockage/alpine/blob/master/3.9/openrc/Dockerfile

## Auto-login
* [alpine linux auto login](https://google.com/search?q=alpine+linux+auto+login)

## Official documentation
* [*Alpine Source Map by boot sequence*
  ](https://wiki.alpinelinux.org/wiki/Alpine_Source_Map_by_boot_sequence)

## Unofficial documentation
* [*Using Runit in a Docker Container*
  ](https://sourcediver.org/blog/2014/11/17/using-runit-in-a-docker-container/)
  2014-11
* [*How to automatically restart a linux background process if it fails?*
  ](https://superuser.com/questions/507576/how-to-automatically-restart-a-linux-background-process-if-it-fails)
  2012-11
* [*Learn the right way to build your Dockerfile*
  ](http://phusion.github.io/baseimage-docker/)
  (phusion/baseimage)
  * init
  * syslog(-ng)
  * cron
  * runit
  * https://hub.docker.com/r/phusion/baseimage
* dockage/alpine docker image