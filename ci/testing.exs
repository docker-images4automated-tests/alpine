require DockerFactory

DockerFactory.make_alpine_images(
    tag: "testing",
    repositories_to_add: [
        "http://dl-cdn.alpinelinux.org/alpine/edge/testing"
    ]

)
